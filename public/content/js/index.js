$(function(){

	$('.select-trpl-click').on('click', function(e){
		if(e.detail === 3){
			e.preventDefault();
			const range = document.createRange();
			range.selectNodeContents(this);

			const selectionObj = window.getSelection();
			selectionObj.removeAllRanges();
			selectionObj.addRange(range);
		}
	})
})