const EVENT_SWITCHING_HIDING = 'track:switching-hiding';
const EVENT_SWITCHING_HIDDEN = 'track:switching-hidden';
const EVENT_SWITCHING_SHOWING = 'track:switching-showing';
const EVENT_SWITCHING_SHOWN   = 'track:switching-shown';

const EVENT_SHOWING = 'track:showing';
const EVENT_SHOWN   = 'track:shown';
const EVENT_HIDING = 'track:hiding';
const EVENT_HIDDEN = 'track:hidden';

const EVENT_DATA_SYNCED = 'track:synced'
const EVENT_TRACK_NO_CHANGE = 'track:no-change'

const widgetStates = {
	visible : false,
	trackId : null,
}

let trackData = {
	'id'       : null,
	'title'    : null,
	'artists'  : null,
	'cover'    : null,
	'duration' : null,
	'progress' : null,
	'playing'  : false
};

$(function(){
	const wrapper = $('#wrapper');
	const container = $('#container');
	const wrapperBg = $('#wrapper-bg');
	const artistsSpan = $('#artists span.text-content');
	const titleSpan = $('#title span.text-content');
	const coverImg = $('#cover > img');

	const userId = container.attr('data-id');

	getNowPLaying();

	function getNowPLaying(){
		$.get({
			url : BASE_URL+"/"+userId+"/widget/api",
			error: function(){
				setTimeout(getNowPLaying, 5000);
			},
			success: function(resp){
				trackData = extractInfo(resp);

				wrapper.trigger(EVENT_DATA_SYNCED);

				if(trackData.playing){
					if(widgetStates.visible){
						if(trackData.id !== widgetStates.trackId){
							if(trackData.cover !== coverImg.attr('src')){
								const img = new Image();
								img.onload = function(){
									switchTrack()
								}
								img.src = trackData.cover;
							} else {
								switchTrack();
							}
						} else {
							wrapper.trigger(EVENT_TRACK_NO_CHANGE);
						}
					} else {
						showWidget(trackData);
					}
				} else if(widgetStates.visible) {
					hideWidget()
				}

				let delay;
				if(trackData.progress !== null && trackData.duration !== null) {
					delay = Math.min(5000, trackData.duration - trackData.progress);
				} else {
					delay = 5000;
				}

				setTimeout(getNowPLaying, delay);
			}
		});
	}


	wrapper
		.on(EVENT_SWITCHING_HIDDEN, function(){
			setWidgetData();
		})
		.on(EVENT_SHOWING, function(){
			setWidgetData()
			widgetStates.visible = true;
		})
		.on(EVENT_HIDING, function(){
			widgetStates.visible = false;
		})

	/*wrapper.on([
		EVENT_HIDING,
		EVENT_HIDDEN,
		EVENT_SHOWING,
		EVENT_SHOWN,
		EVENT_SWITCHING_HIDING,
		EVENT_SWITCHING_HIDDEN,
		EVENT_SWITCHING_SHOWING,
		EVENT_SWITCHING_SHOWN,
		EVENT_DATA_SYNCED,
		EVENT_TRACK_NO_CHANGE
	].join(' '), function(event) {
		console.log(event.type)
	})*/

	wrapperBg.on('transitioncancel transitionend', function(){
		$(this).addClass('hidden')
	})

	function showWidget(data = null){
		wrapper.trigger(EVENT_SHOWING);

		const img = new Image();
		img.onload = function(){
			wrapperBg.one('transitioncancel transitionend', function(){ // Slide In
				wrapperBg.one('transitioncancel transitionend', function(){ //slide out (révèle le widget)
					wrapper.trigger(EVENT_SHOWN);
				})
			})

			wrapperBg.removeClass('hidden');
			container.removeClass('hidden');
		}
		img.src = data.cover;
	}

	function hideWidget(){
		wrapper.trigger(EVENT_HIDING);

		wrapperBg.one('transitioncancel transitionend', function(){
			wrapperBg.one('transitioncancel transitionend', function(){
				wrapper.trigger(EVENT_HIDDEN);
			})

			container.addClass('hidden');
		})
		wrapperBg.removeClass('hidden');

	}

	function switchTrack(){
		wrapper.trigger(EVENT_SWITCHING_HIDING);

		wrapperBg.one('transitioncancel transitionend', function(){
			wrapper.trigger(EVENT_SWITCHING_HIDDEN)
			       .trigger(EVENT_SWITCHING_SHOWING);

			wrapperBg.one('transitioncancel transitionend', function(){
				wrapper.trigger(EVENT_SWITCHING_SHOWN);
			});
		});
		wrapperBg.removeClass('hidden');
	}





	function extractInfo(data){
		if(data && data !== ""){
			return {
				'id'       : data.item.id,
				'title'    : data.item.name,
				'artists'  : data.item.artists.map((value) => value.name).join(', '),
				'cover'    : data.item.album.images[0].url,
				'duration' : data.item.duration_ms,
				'progress' : data.progress_ms,
				'playing'  : data.is_playing && data.item
			}
		} else {
			return {
				'id'       : null,
				'title'    : null,
				'artists'  : null,
				'cover'    : null,
				'duration' : null,
				'progress' : null,
				'playing'  : false
			}
		}
	}




	function setWidgetData(){
		setTitle(trackData.title || '');
		setArtists(trackData.artists || '');
		setCover(trackData.cover || '');
		widgetStates.trackId = trackData.id;
	}
	function setArtists(value){
		artistsSpan.text(value)
		checkScroll($('#artists'))
	}
	function setTitle(value){
		titleSpan.text(value)
		checkScroll($('#title'))
	}
	function setCover(value){
		const currentSrc = coverImg.attr('src');
		if(value !== currentSrc){
			coverImg.attr('src', value);
		}
	}

	function checkScroll(element){
		const span = element.find('span.text-content')
		element.removeClass('scroll');
		span.attr('data-content', '')

		setTimeout(() => {
			if(span.width() > element.width()){
				span.attr('data-content', span.text())
				element.addClass('scroll');
			}
		})
	}
})
