$(function(){
	const wrapper = $('#wrapper');
	const progressbar = $('#track-progress');

	wrapper
		.on([EVENT_TRACK_NO_CHANGE, EVENT_SWITCHING_HIDDEN, EVENT_SHOWING].join(' '), function(){
			updateProgress(trackData.progress, trackData.duration);
		})

	function updateProgress(progress, duration){
		progressbar.stop(true)
		           .css('width', (progress*100/duration)+'%')
		           .animate({'width': '100%'}, duration-progress, 'linear');
	}
})
