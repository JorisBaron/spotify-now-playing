# Spotify Now Playing

Application permettant d'obtenir un widget affichant votre musique actuelle de Spotify, affichable dans votre logiciel de streaming préféré !

---

Build with [CodeIgniter](https://www.codeigniter.com/), 
[Spotify Web API PHP](https://github.com/jwilsson/spotify-web-api-php), 
[JsonDB](https://github.com/donjajo/php-jsondb) & 
[Bootstrap](https://getbootstrap.com/)
