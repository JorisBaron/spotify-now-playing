<?php

namespace App\Repository;

use App\Entity\SpotifyUser;
use Jajo\JSONDB;

class SpotifyUsersRepository {
	private $db;
	private $file = 'users.json';

	public function __construct(){
		$this->db = new JSONDB(WRITEPATH.'jsondb/');
	}

	public function find($userId): ?SpotifyUser {
		$res = $this->db
			->select()->from($this->file)
			->where([
				'id' => $userId
			])
			->get();

		if(empty($res)){
			return null;
		} else {
			return new SpotifyUser($res[0]);
		}
	}

	public function findSpotifyId($spotifyId): ?SpotifyUser {
		$res = $this->db
			->select()->from($this->file)
			->where([
				'spotifyId' => $spotifyId
			])
			->get();

		if(empty($res)){
			return null;
		} else {
			return new SpotifyUser($res[0]);
		}
	}

	public function exists($userId): bool {
		return $this->find($userId) !== null;
	}

	public function existsSpotifyId($spotifyId): bool {
		return $this->findSpotifyId($spotifyId) !== null;
	}

	public function save(SpotifyUser &$user){
		if($this->existsSpotifyId($user->spotifyId)){
			$user->id = $this->findSpotifyId($user->spotifyId)->id;
			$this->db
				->update([
					'accessToken'  => $user->accessToken,
					'refreshToken' => $user->refreshToken,
				])
				->from($this->file)
				->where([
					'spotifyId' => $user->spotifyId
				])
				->trigger();
		}
		else {
			$this->db->insert($this->file, $user->toArray());
		}
	}
}