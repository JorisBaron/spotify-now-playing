<?php

namespace Config;

use App\Controllers\Home;
use App\Repository\SpotifyUsersRepository;
use App\Service\SpotifyApiService;
use CodeIgniter\Config\BaseService;
use SpotifyWebAPI\Session;
use SpotifyWebAPI\SpotifyWebAPI;

/**
 * Services Configuration file.
 *
 * Services are simply other classes/libraries that the system uses
 * to do its job. This is used by CodeIgniter to allow the core of the
 * framework to be swapped out easily without affecting the usage within
 * the rest of your application.
 *
 * This file holds any application-specific services, or service overrides
 * that you might need. An example has been included with the general
 * method format you should use for your service methods. For more examples,
 * see the core Services file at system/Config/Services.php.
 */
class Services extends BaseService
{
	// public static function example($getShared = true)
	// {
	//     if ($getShared)
	//     {
	//         return static::getSharedInstance('example');
	//     }
	//
	//     return new \CodeIgniter\Example();
	// }

	public static function spotifyUserRepository($getShared = true) : SpotifyUsersRepository {

		if($getShared) {
			return static::getSharedInstance('spotifyUserRepository');
		}

		return new SpotifyUsersRepository();
	}

	public static function spotifySession($getShared = true): Session{
		if($getShared) {
			return static::getSharedInstance('spotifySession');
		}

		return new Session(
			getenv('spotify.client_id'),
			getenv('spotify.client_secret'),
			base_url(route_to(Home::class.'::login'))
		);
	}


	public static function spotifyApi(Session $session = null, $getShared = true): SpotifyWebAPI {
		if($getShared) {
			return static::getSharedInstance('spotifyApi', $session);
		}

		return new SpotifyWebAPI([
			'auto_refresh' => true,
			'auto_retry'   => true,
		], $session);
	}

	public static function spotifyApiService($getShared = true) {
		if($getShared) {
			return static::getSharedInstance('spotifyApiService');
		}

		return new SpotifyApiService();
	}
}
