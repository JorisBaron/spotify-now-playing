<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;
use Config\Services;

class Home extends BaseController
{
	use ResponseTrait;

	public function index() {
		echo view('templates/start');
		echo view('index');
		echo view('templates/end');
	}

	public function login(){
		if(!isset($_GET['code'])){
			return Services::spotifyApiService()->generateAuthRedirect();
		} else {
			$userId = Services::spotifyApiService()->processApiCallback($_GET);

			return redirect()->route('home', [$userId]);
		}
	}

	public function home(string $userId){
		echo view('templates/start');
		echo view('home', [
			'userId' => $userId,
		]);
		echo view('templates/end');
	}

	public function widget($userId){
		$exists = Services::spotifyUserRepository()->exists($userId);
		if(!$exists){
			return $this->response->setStatusCode(404)->setBody('User not found');
		}

		if (!$this->validate([
			'side'     => "permit_empty|in_list[left,right]",
			'progress' => "permit_empty|in_list[under,bar,background,bg]",
		])) {
			var_dump($this->validator->getErrors());
			return $this->response->setStatusCode(400)->setBody('Invalid parameters');
		}

		$styles = [
			'/content/css/widget.css',
			'/content/css/widget-'. ($this->request->getGet('side') ?? 'left').'.css',
		];
		$scripts = [
			'/content/js/widget.js',
		];

		$progress = $this->request->getGet('progress');
		if($progress) {
			$view = 'widget-progress';
			$scripts[] = '/content/js/widget-progress.js';

			switch($progress){
				case 'under' :
				case 'bar' :
					$styles[]  = '/content/css/widget-progress.css';
					break;
				case 'bg':
				case 'background':
					$styles[]  = '/content/css/widget-progress-bg.css';
					break;
			}
		} else {
			$view = 'widget';
		}

		echo view('templates/start', [
			'styles' => $styles
		]);
		echo view($view, [
			'userId' => $userId
		]);
		echo view('templates/end', [
			'scripts' => $scripts
		]);
	}

	public function chatApi($userId){
		$exists = Services::spotifyUserRepository()->exists($userId);
		if(!$exists){
			return $this->failNotFound('Erreur avec le bot (User not found)');
		}

		$apiService = Services::spotifyApiService();
		$apiInstance = $apiService->getApiInstance($userId);

		$data = $apiInstance->api->getMyCurrentTrack();

		if(empty($data) || !$data->is_playing){
			return $this->respond('Aucune musique en cours', 200);
		}

		$artists = array_map(fn($artist) => $artist->name, $data->item->artists);

		$apiService->saveTokens($apiInstance);
		return $this->respond('"'.$data->item->name.'" — '.implode(', ', $artists));
	}

	public function widgetApi($userId){
		$this->setResponseFormat('json');

		$exists = Services::spotifyUserRepository()->exists($userId);
		if(!$exists) {
			return $this->failNotFound('User not found');
		}

		$apiService = Services::spotifyApiService();
		$apiInstance = $apiService->getApiInstance($userId);

		$data = $apiInstance->api->getMyCurrentTrack();

		$apiService->saveTokens($apiInstance);

		return $this->respond($data,200);
	}
}
