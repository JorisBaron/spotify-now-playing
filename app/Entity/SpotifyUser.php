<?php

namespace App\Entity;

class SpotifyUser {

	public $id;
	public $spotifyId;
	public $accessToken;
	public $refreshToken;

	public function __construct(array $data = []) {
		$this->setData($data);
	}

	public function setData(array $data = []): void {
		foreach(['id','spotifyId','accessToken','refreshToken'] as $key){
			if(isset($data[$key])){
				$this->$key = $data[$key];
			}
		}
	}

	public function toArray(): array {
		$arr = [];
		foreach(['id','spotifyId','accessToken','refreshToken'] as $key){
			$arr[$key] = $this->$key;
		}
		return $arr;
	}
}