<div class="h-100 d-flex justify-content-center align-items-center">
	<div id="wrapper" class="position-relative overflow-hidden">
		<div id="container" class="d-inline-block position-relative overflow-hidden hidden"
			 data-id="<?= $userId ?>">
			<aside id="cover" class="position-absolute">
				<img alt="Album cover" src="" class="h-100 w-100">
			</aside>
			<main id="main" class="position-absolute">
				<div id="track-progress" class="position-absolute"></div>
				<div id="text-wrapper" class="position-relative overflow-hidden">
					<h4 id="artists" class="text-nowrap overflow-hidden">
						<span class="text-wrapper d-inline-block text-nowrap position-relative">
							<span class="d-inline-block text-nowrap text-content" data-content=""></span>
						</span>
					</h4>
					<h2 id="title" class="text-nowrap overflow-hidden">
						<span class="text-wrapper d-inline-block text-nowrap position-relative">
							<span class="d-inline-block text-nowrap text-content" data-content=""></span>
						</span>
					</h2>
				</div>
			</main>
		</div>
		<div id="wrapper-bg" class="position-absolute hidden"></div>
	</div>
</div>