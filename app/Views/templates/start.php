<!DOCTYPE html>
<html lang="fr" class="h-100">
	<head>
		<meta charset="UTF-8">
		<title><?= $pageTitle ?? 'Now Playing Spotify - by JooJ.io' ?></title>
		<meta name="description" content="Spotify Now Playing">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" type="image/png" href="/favicon.ico"/>

		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
			  integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

		<?php if(isset($styles)) : ?>
			<?php foreach($styles as $style): ?>
				<link href="<?=$style?>" rel="stylesheet">
			<?php endforeach; ?>
		<?php endif; ?>

	</head>
	<body class="h-100">