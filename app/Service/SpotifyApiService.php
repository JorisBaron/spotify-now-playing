<?php

namespace App\Service;

use App\Entity\SpotifyUser;
use CodeIgniter\HTTP\RedirectResponse;
use Config\Services;
use Ramsey\Uuid\Uuid;
use SpotifyWebAPI\Session;
use SpotifyWebAPI\SpotifyWebAPI;

class SpotifyApiService {
	public function getSession(): Session {
		return Services::spotifySession();
	}

	public function getWebApi(Session $session = null): SpotifyWebAPI {
		return Services::spotifyApi($session);
	}

	public function generateAuthRedirect(): RedirectResponse {
		$spotifySession = Services::spotifySession();
		$session = session();

		$state = $spotifySession->generateState();
		$session->set('spotify.state', $state);

		$options = [
			'scope' => [
				'user-read-currently-playing',
				'user-read-playback-state',
			],
			'state' => $state,
		];

		return redirect()->to($spotifySession->getAuthorizeUrl($options));
	}

	public function processApiCallback($getData){
		$spotifySession = Services::spotifySession();

		$state = $getData['state'];

		// Fetch the stored state value from somewhere. A session for example

		if ($state !== session('spotify.state')) {
			// The state returned isn't the same as the one we've stored, we shouldn't continue
			die('State mismatch');
		}

		// Request a access token using the code from Spotify
		$spotifySession->requestAccessToken($getData['code']);

		$api = Services::spotifyApi($spotifySession);
		$spotifyId = $api->me()->id;

		$userRepo = Services::spotifyUserRepository();

		$user = new SpotifyUser();
		$user->id = Uuid::uuid4()->toString();
		$user->spotifyId = $spotifyId;
		$user->accessToken = $spotifySession->getAccessToken();
		$user->refreshToken = $spotifySession->getRefreshToken();

		$userRepo->save($user);

		return $user->id;
	}

	public function getApiInstance($userId): ?SpotifyApiInstance {
		$user = Services::spotifyUserRepository()->find($userId);
		if(!$user){
			return null;
		}

		$session = Services::spotifySession(false);
		$session->setAccessToken($user->accessToken);
		$session->setRefreshToken($user->refreshToken);

		$api = Services::spotifyApi($session, false);

		return new SpotifyApiInstance($user, $api, $session);
	}

	public function saveTokens(SpotifyApiInstance $apiInstance){
		$user = $apiInstance->user;
		$user->accessToken = $apiInstance->session->getAccessToken();
		$user->refreshToken = $apiInstance->session->getRefreshToken();
		Services::spotifyUserRepository()->save($user);
	}
}