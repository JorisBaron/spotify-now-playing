<?php

namespace App\Service;

use App\Entity\SpotifyUser;
use SpotifyWebAPI\Session;
use SpotifyWebAPI\SpotifyWebAPI;

/**
 * @property-read SpotifyUser $user
 * @property-read SpotifyWebAPI $api
 * @property-read Session $session
 */
class SpotifyApiInstance {
	private $user;
	private $api;
	private $session;

	public function __construct(SpotifyUser $user, SpotifyWebAPI $api, Session $session) {
		$this->user = $user;
		$this->api = $api;
		$this->session = $session;
	}

	public function __get($name) {
		return $this->$name;
	}
}